

#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np
drawing = False  # true if mouse is pressed
mode = True  # if True, draw rectangle. Press ’m’ to toggle to curve
ix, iy = -1, -1

def draw_circle(event, x, y, flags, param):
    global ix, iy, drawing, mode, img_copia
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix, iy = x, y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True:
            if mode is True :
                cv2.rectangle( img, (ix, iy), (x, y),(0, 255, 0), -1)
            elif mode is False:
                cv2.circle(img, (x, y), 5, (0, 0, 255), -1)

    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        if mode is True:
            cv2.rectangle(img, (ix, iy), (x, y), (0, 255, 0), -1)
        else:
            cv2.circle(img, (x, y), 5, (0, 0, 255), -1)
        if ix < x:
            if iy < y:
                img_copia = sub_img[iy:y, ix:x]
            else:
                img_copia = sub_img[y:iy, ix:x]
        else:
            if iy < y:
                img_copia = sub_img[iy:y, x:ix]
            else:
                img_copia = sub_img[y:iy, x:ix]

    print(y, x, iy, ix)

img=cv2.imread('hoja.png')
sub_img=img.copy()
cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_circle)
while 1:
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('m'):
        mode = not mode
    elif k == 27:
        break
    elif k == ord('g'):
        #cv2.imwrite('copia.png', img_copia)
        cv2.imshow('image_copia', img_copia)
    elif k == ord('r'):
        img = sub_img.copy()
    elif k == ord ('q'):
        break
cv2.destroyAllWindows( )
