#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np
drawing = False  # true if mouse is pressed
mode = True  # if True, draw rectangle. Press ’m’ to toggle to curve
ix, iy = -1, -1
contador = 0
A = [0, 0]
B = [0, 0]
C = [0, 0]
def draw_circle(event, x, y, flags, param):
    global ix, iy, drawing, mode, img_copia, contador, A, B, C
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix, iy = x, y
        if contador == 0:
            A = [ix, iy]
        elif contador == 1:
            B = [ix, iy]
        elif contador == 2:
            C = [ix, iy]
            contador = -1
        contador += 1
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True:
            if mode is True :
                cv2.rectangle( img, (ix, iy), (x, y),(0, 255, 0), -1)
            elif mode is False:
                cv2.circle(img, (x, y), 5, (0, 0, 255), -1)

    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False

        if mode is True:
            cv2.rectangle(img, (ix, iy), (x, y), (0, 255, 0), -1)
        else:
            cv2.circle(img, (x, y), 5, (0, 0, 255), -1)
        if ix < x:
            if iy < y:
                img_copia = sub_img[iy:y, ix:x]
            else:
                img_copia = sub_img[y:iy, ix:x]
        else:
            if iy < y:
                img_copia = sub_img[iy:y, x:ix]
            else:
                img_copia = sub_img[y:iy, x:ix]


def transformacion_afin():
    print(A)
    print(B)
    print(C)
    imagen2 = cv2.imread('roman.jpeg')  #225x225
    w = img.shape[1]
    h = img.shape[0]

    img_vacia = np.zeros((625, 625, 3), np.uint8)
    src = np.float32([[0, 0], [imagen2.shape[1], 0], [0, imagen2.shape[0]]])
    dst = np.float32([A, B, C])
    matrix = cv2.getAffineTransform(src, dst)
    img_nueva = cv2.warpAffine(imagen2, matrix, (h, w))

    for i in range(h):
        for j in range(w):
            if np.all(img_nueva[i][j]) < 1:
                img_nueva[i][j] = img[i][j]
    cv2.imwrite('imagenafin.png', img_nueva)


img = cv2.imread('hoja.png')
sub_img = img.copy()
cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_circle)
while 1:
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('m'):
        mode = not mode
    elif k == 27:
        break
    elif k == ord('g'):
        cv2.imwrite('copia.png', img_copia)
        cv2.imshow('image_copia', img_copia)
    elif k == ord('r'):
        img = sub_img.copy()
    elif k == ord('q'):
        break
    elif k == ord('a'):
        transformacion_afin()
        imagenafin = cv2.imread('imagenafin.png')
        cv2.imshow('', imagenafin)
cv2.destroyAllWindows( )
