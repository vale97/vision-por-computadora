import random
def adivina(intentos):
	numero=random.randrange(0,10,1)
	i=0
	while intentos != i:
		while True:
			entrada=input('ingrese numero: ')
			if entrada.isnumeric():
				break
			else:
				print('ingrese un numero valido')
		entrada=int(entrada)		
		if entrada == numero:		
			print('Adivinaste el numero!!')
			return i
		else:
			if entrada < numero:
				print('Los numeros son distintos, el numero secreto es mayor\n')
			else:
				print('Los numeros son distinto, el numero secreto es menor\n')
		i += 1
  
	print('el numero era: ' ,numero)

while True:
	i=input('ingrese la cantidad de intentos: ')	
	if i.isnumeric():	
		break
	else:
		print('Ingrese un numero de intentos valido')

i=int(i)
intentos=adivina(i)

if intentos != None:
	print('Lo adivinaste en el intento: ',intentos+1)

else:
	print('Te quedaste sin intentos')

