#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np
import math

drawing = False  # true if mouse is pressed
mode = True  # if True, draw rectangle. Press ’m’ to toggle to curve
ix, iy = -1, -1


def draw_circle(event, x, y, flags, param):
    global ix, iy, drawing, mode, img_copia
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix, iy = x, y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True:
            if mode is True:
                cv2.rectangle(img, (ix, iy), (x, y), (0, 255, 0), -1)
            elif mode is False:
                cv2.circle(img, (x, y), 5, (0, 0, 255), -1)

    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        if mode is True:
            cv2.rectangle(img, (ix, iy), (x, y), (0, 255, 0), -1)
        else:
            cv2.circle(img, (x, y), 5, (0, 0, 255), -1)
        if ix < x:
            if iy < y:
                img_copia = sub_img[iy:y, ix:x]
            else:
                img_copia = sub_img[y:iy, ix:x]
        else:
            if iy < y:
                img_copia = sub_img[iy:y, x:ix]
            else:
                img_copia = sub_img[y:iy, x:ix]


def vec_homo():
    z = 1
    img_x = cv2.imread('hoja-copia.png')
    img_dest = np.zeros((500, 500, 3))
    y = img_x.shape[0]
    x = img_x.shape[1]
    ang= math.radians(90)
    for j in range(y):
        for i in range(x):
            vec = np.float32([[j],
                   [i],
                   [z]])
            M = np.float32([[math.cos(ang), math.sin(ang), 100],
                 [-math.sin(ang), math.cos(ang), 100],
                 [0, 0, 1]])
            vec_1 = np.matmul(M, vec)
            print(int(vec_1[1, 0]), int(vec_1[0, 0]))
            img_dest[int(vec_1[0, 0])][int(vec_1[1, 0])][:] = img_x[j][i][:]


    cv2.imwrite('hoja-trans.png', img_dest)
    cv2.imshow('imagen-transformada', img_dest)


img = cv2.imread('hoja.png')
sub_img = img.copy()
cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_circle)
while 1:
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('m'):
        mode = not mode
    elif k == 27:
        break
    elif k == ord('g'):
        cv2.imwrite('hoja-copia.png', img_copia)
        cv2.imshow('hoja-copia', img_copia)
    elif k == ord('r'):
        img = sub_img.copy()
    elif k == ord('q'):
        break
    elif k == ord('e'):
        vec_homo()

cv2.destroyAllWindows()

# hacer transformacion homogenea del punto, y luego multiplicar vectorialmente con la matriz de transformacion
