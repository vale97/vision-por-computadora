#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np
import math

drawing = False  # true if mouse is pressed
mode = True  # if True, draw rectangle. Press ’m’ to toggle to curve
ix, iy = -1, -1
img_copia = np.zeros((1, 1, 3))
jy, jx = -1, -1

def draw_circle(event, x, y, flags, param):
    global ix, iy, drawing, mode, img_copia, jx, jy
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix, iy = x, y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True:
            if mode is True:
                cv2.rectangle(img, (ix, iy), (x, y), (0, 255, 0), -1)
            elif mode is False:
                cv2.circle(img, (x, y), 5, (0, 0, 255), -1)

    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        if mode is True:
            cv2.rectangle(img, (ix, iy), (x, y), (0, 255, 0), -1)
        else:
            cv2.circle(img, (x, y), 5, (0, 0, 255), -1)
        if ix < x:
            if iy < y:
                img_copia = sub_img[iy:y, ix:x]
                jy = y
                jx = x
            else:
                img_copia = sub_img[y:iy, ix:x]
                jy = iy
                iy = y
                jx = x
        else:
            if iy < y:
                img_copia = sub_img[iy:y, x:ix]
                jy = y
                jx = ix
                ix = x
            else:
                img_copia = sub_img[y:iy, x:ix]
                jy = iy
                iy = y
                jx = ix
                ix = x


def transformacion_euclidiana():
    img_x = img_copia

    h = img.shape[0]
    w = img.shape[1]
    img_dest = np.zeros((h, w, 3))
    y = img_x.shape[0]
    x = img_x.shape[1]

    ang = math.radians(-30)
    for j in range(y):
        for i in range(x):
            vec = np.float32([[j+iy],
                              [i+ix],
                              [1]])
            M = np.float32([[math.cos(ang), math.sin(ang), 50],
                            [-(math.sin(ang)), math.cos(ang), 50]])
            vec_1 = np.matmul(M, vec)
            img_dest[int(vec_1[0, 0])][int(vec_1[1, 0])][:] = img_x[j][i][:]

    cv2.imwrite('hoja-trans.png', img_dest)


def transformacion_rapida_eu():
    s = 0.5
    h = img.shape[0]
    w = img.shape[1]
    y = img_copia.shape[0]
    x = img_copia.shape[1]
    img_dest1 = np.zeros((h, w, 3))
    img_dest1[iy:iy+y, ix:ix+x] = img_copia[0:y,0:x]
    ang = math.radians(30)

    M = np.float32([[s*math.cos(ang), s*math.sin(ang), 50],
                    [s*-(math.sin(ang)), s*math.cos(ang), 50]])

    img_c = cv2.warpAffine(img_dest1, M, (w,h))

    cv2.imwrite('hoja-trans-rapida.png', img_c)


img = cv2.imread('hoja.png')
sub_img = img.copy()
cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_circle)
while 1:
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('m'):
        mode = not mode
    elif k == 27:
        break
    elif k == ord('g'):
        cv2.imwrite('hoja-copia.png', img_copia)
        cv2.imshow('hoja-copia', img_copia)
    elif k == ord('r'):
        img = sub_img.copy()
    elif k == ord('q'):
        break
    elif k == ord('e'):
        transformacion_euclidiana()
        transformacion_rapida_eu()
        img_euclidiana = cv2.imread('hoja-trans-rapida.png')
        img_euclidiana_normal = cv2.imread('hoja-trans.png')
        Horizontal = np.concatenate((img_euclidiana, img_euclidiana_normal), axis=1)
        cv2.imshow('', Horizontal)

cv2.destroyAllWindows()

# hacer transformacion homogenea del punto, y luego multiplicar vectorialmente con la matriz de transformacion
