import cv2 as cv

import numpy as np
# Cargamos el diccionario.
dictionary = cv.aruco.Dictionary_get(cv.aruco.DICT_6X6_250)
# Creamos el detector
parameters = cv.aruco.DetectorParameters_create()
cap = cv.VideoCapture(0)
fourcc = cv.VideoWriter_fourcc('X', 'V', 'I', 'D')
fps = 20
out = cv.VideoWriter('aruco.avi', fourcc, fps, (640, 480))


def detect_img(im_src):
    global out
    while 1:
        ret, frame = cap.read()
        # Detectamos los marcadores en la imagen
        Corners, Ids, rejected = cv.aruco.detectMarkers(frame, dictionary, parameters=parameters)

        if Ids is not None:
            #print("")
            #print("esquinas:{}".format(Corners))
            #print("ids:{}".format(ids))
            button = cv.imread("pulsador.jpeg")
            if len(Ids) == 5:
                index = np.squeeze(np.where(Ids == 1))                         #se busca por Id
                refPt1 = np.squeeze(Corners[index[0]])[0]

                index = np.squeeze(np.where(Ids == 2))
                refPt2 = np.squeeze(Corners[index[0]])[0]                      #se utiliza el origien del eje de coordenadas

                distance = np.linalg.norm(refPt1 - refPt2)

                scalingFac = 0.02                                              #factor de escala para que no se vean los bordes de la impresion
                pts_dst = [[refPt1[0] - round(scalingFac * distance), refPt1[1] - round(scalingFac * distance)]]
                pts_dst = pts_dst + [[refPt2[0] + round(scalingFac * distance), refPt2[1] - round(scalingFac * distance)]]

                index = np.squeeze(np.where(Ids == 3))
                refPt3 = np.squeeze(Corners[index[0]])[0]
                pts_dst = pts_dst + [[refPt3[0] + round(scalingFac * distance), refPt3[1] + round(scalingFac * distance)]]

                index = np.squeeze(np.where(Ids == 4))
                refPt4 = np.squeeze(Corners[index[0]])[0]
                pts_dst = pts_dst + [[refPt4[0] - round(scalingFac * distance), refPt4[1] + round(scalingFac * distance)]]

                pts_src = [[0, 0], [im_src.shape[1], 0], [im_src.shape[1], im_src.shape[0]], [0, im_src.shape[0]]]

                pts_src_m = np.asarray(pts_src)
                pts_dst_m = np.asarray(pts_dst)

                #deteccion de esquinas del pulsador
                index = np.squeeze(np.where(Ids == 5))  # se busca por Id
                BPt1 = np.squeeze(Corners[index[0]])[0]
                BPt2 = np.squeeze(Corners[index[0]])[1]
                BPt3 = np.squeeze(Corners[index[0]])[2]
                BPt4 = np.squeeze(Corners[index[0]])[3]

                button_src = [[0, 0], [button.shape[1], 0], [button.shape[1], button.shape[0]], [0, button.shape[0]]]
                button_dst = [[BPt1], [BPt2], [BPt3], [BPt4]]
                button_dst_m = np.squeeze(np.asarray(button_dst))
                m, estado = cv.findHomography(np.asarray(button_src), np.asarray(button_dst))
                warped_image_1 = cv.warpPerspective(button, m, (frame.shape[1], frame.shape[0]))
                warped_image_1 = warped_image_1.astype(float)

                h, status = cv.findHomography(pts_src_m, pts_dst_m)
                warped_image = cv.warpPerspective(im_src, h, (frame.shape[1], frame.shape[0]))

                mask = np.zeros([frame.shape[0], frame.shape[1]])

                cv.fillConvexPoly(mask, np.int32([pts_dst_m]), (255, 255, 255), cv.LINE_AA)     #mascara blanca dentro del recuadro de la imagen
                cv.fillConvexPoly(mask, np.int32([button_dst_m]), (255, 255, 255), cv.LINE_AA)
                warped_image = warped_image.astype(float)
                mask3 = np.zeros_like(warped_image)    #mascara negra del tamaño de resolucion de la camara
                for i in range(0, 3):
                    mask3[:, :, i] = mask/255

                warped_image_masked = cv.multiply(warped_image, mask3)
                warped_image_masked_1= cv.multiply(warped_image_1, mask3)
                frame_masked = cv.multiply(frame.astype(float), 1 - mask3)
                im_out = cv.add(warped_image_masked, frame_masked)

                im_aruco = cv.add(im_out, warped_image_masked_1)

                cv.imshow("AR using Aruco markers", im_aruco.astype(np.uint8))
                out.write(im_aruco.astype(np.uint8))


        # Dibujamos los marcadores detectados en la imagen
        frame = cv.aruco.drawDetectedMarkers(frame, Corners, Ids)
        cv.imshow("ventana", frame)
        if Ids is not None and len(Ids) == 4:
            cv.waitKey(1000)
            if 5 not in Ids:

                break

        key = cv.waitKey(20)
        if key == 27:
            return key


while 1:
    img = cv.imread("kempes.jpg")
    key = detect_img(img)
    if key == 27:
        break
    img = cv.imread("maradona.jpg")
    key = detect_img(img)
    if key == 27:
        break
    img = cv.imread("roman.jpg")
    key = detect_img(img)
    if key == 27:
        break
    img = cv.imread("messi.jpg")
    key = detect_img(img)
    if key == 27:
        break


out.release()
cap.release()

cv.destroyAllWindows()

