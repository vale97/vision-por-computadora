#! /usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import cv2

MIN_MATCH_COUNT = 10

img1 = cv2.imread('monitor1.jpeg')
img2 = cv2.imread('monitor2.jpeg')
img_h = img1.shape[0]
img_w = img1.shape[1]

sift = cv2.SIFT_create()
kp1, des1 = sift.detectAndCompute(img1, None)
kp2, des2 = sift.detectAndCompute(img2, None)

matcher = cv2.BFMatcher(cv2.NORM_L2)
matches = matcher.knnMatch(des1, des2, k=2)

# Guardamos los buenos matches usando el test de razón de Lowe
good = []
for m, n in matches:
    if m.distance < 0.7 * n.distance:
        good.append(m)

if len(good) > MIN_MATCH_COUNT:
    src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
    dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
    H, mask = cv2.findHomography(dst_pts, src_pts, cv2.RANSAC, 5.0)  # Computamos la homografía con RANSAC
else:
    print("No tiene buenos matches")

wimg2 = cv2.warpPerspective(img2, H, (img_w, img_h))  # Aplicamos la transformación perspectiva H a la imagen 2

# Mezclamos ambas imágenes
alpha = 0.5

blend = np.array(wimg2*alpha+img1*(1-alpha), dtype=np.uint8)


cv2.imshow('Imagen', blend)
cv2.waitKey(0)

cv2.destroyAllWindows()
