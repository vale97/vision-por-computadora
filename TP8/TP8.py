#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np
drawing = False  # true if mouse is pressed
mode = True  # if True, draw rectangle. Press ’m’ to toggle to curve
ix, iy = -1, -1
contador = 0
A = [0, 0]
B = [0, 0]
C = [0, 0]
D = [0, 0]


def draw_circle(event, x, y, flags, param):
    global ix, iy, drawing, mode, img_copia, contador, A, B, C, D
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix, iy = x, y
        if contador == 0:
            A = [ix, iy]
        elif contador == 1:
            B = [ix, iy]
        elif contador == 2:
            C = [ix, iy]
        elif contador == 3:
            D = [ix, iy]
            contador = -1
        contador += 1
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True:
            if mode is True:
                cv2.rectangle(img, (ix, iy), (x, y), (0, 255, 0), -1)
            elif mode is False:
                cv2.circle(img, (x, y), 5, (0, 0, 255), -1)

    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False

        if mode is True:
            cv2.rectangle(img, (ix, iy), (x, y), (0, 255, 0), -1)
        else:
            cv2.circle(img, (x, y), 5, (0, 0, 255), -1)
        if ix < x:
            if iy < y:
                img_copia = sub_img[iy:y, ix:x]
            else:
                img_copia = sub_img[y:iy, ix:x]
        else:
            if iy < y:
                img_copia = sub_img[iy:y, x:ix]
            else:
                img_copia = sub_img[y:iy, x:ix]

def homografia():
    print(A)
    print(B)
    print(C)
    print(D)
    imagen3 = cv2.imread('Dolar.jpg')  # 225x225

    width = np.sqrt((B[0] - A[0])**2 + (B[1] - A[1])**2)
    height = np.sqrt((D[0] - A[0])**2 + (D[1] - A[1])**2)
    src = np.float32([A, B, C, D])
    dst = np.float32([[0, 0], [int(width), 0], [int(width), int(height)],[0, int(height)]])
    matrix = cv2.getPerspectiveTransform(src,dst)

    out = cv2.warpPerspective(imagen3, matrix, (int(width), int(height)))

    cv2.imwrite('homografia.png', out)



img = cv2.imread('Dolar.jpg')
sub_img = img.copy()
cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_circle)
while 1:
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('m'):
        mode = not mode
    elif k == 27:
        break
    elif k == ord('q'):
        break
    elif k == ord('h'):
        homografia()
        img_homografia = cv2.imread('homografia.png')
        cv2.imshow('', img_homografia)
cv2.destroyAllWindows( )
