#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np
drawing = False  # true if mouse is pressed
mode = True  # if True, draw rectangle. Press ’m’ to toggle to curve
medicion = True
ix, iy = -1, -1
contador = 0
Ai = [0, 0]
Bi = [0, 0]
A = [55, 170]
B = [485, 145]
C = [485, 310]
D = [55, 280]
matrix = 0
h_puerta = 100  #aproximadamente 1.7 metros


def medir_dist(i, j):

    p_1 = np.float32([[i[0]],
                     [i[1]],
                     [1]])
    p_2 = np.float32([[j[0]],
                     [j[1]],
                     [1]])
    medida_1 = np.matmul(matrix, p_1)
    medida_2 = np.matmul(matrix, p_2)
    medida_1 = medida_1/medida_1[2]
    medida_2 = medida_2/medida_2[2]

    width = np.sqrt((medida_2[0] - medida_1[0]) ** 2 + (medida_2[1] - medida_1[1]) ** 2)

    medida = round(float((width*2)/h_puerta), 2)
    return medida


def draw_circle(event, x, y, flags, param):
    global ix, iy, drawing, mode, img_copia, contador, Ai, Bi
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix, iy = x, y
        if contador == 0:
            Ai = [ix, iy]
            cv2.circle(img, Ai, 3, (0, 0, 255), -1)

        elif contador == 1:
            Bi = [ix, iy]
            cv2.circle(img, Bi, 3, (0, 0, 255), -1)
            cv2.line(img, Ai, Bi, (0, 0, 255), 2)
            cv2.putText(img, (str(medir_dist(Ai, Bi)) + "m"), Bi, cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
            contador = -1

        contador += 1
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True:
            if mode is True:
                cv2.rectangle(img, (ix, iy), (x, y), (0, 255, 0), -1)
            elif mode is False:
                cv2.circle(img, (x, y), 5, (0, 0, 255), -1)

    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False

        if mode is True:
            cv2.rectangle(img, (ix, iy), (x, y), (0, 255, 0), -1)
        else:
            cv2.circle(img, (x, y), 5, (0, 0, 255), -1)
        if ix < x:
            if iy < y:
                img_copia = sub_img[iy:y, ix:x]
            else:
                img_copia = sub_img[y:iy, ix:x]
        else:
            if iy < y:
                img_copia = sub_img[iy:y, x:ix]
            else:
                img_copia = sub_img[y:iy, x:ix]


def perspectiva():
    global matrix
    imagen3 = cv2.imread('casa.png')  # 225x225

    width = np.sqrt((B[0] - A[0])**2 + (B[1] - A[1])**2)
    height = np.sqrt((D[0] - A[0])**2 + (D[1] - A[1])**2)
    src = np.float32([A, B, C, D])
    dst = np.float32([[0, 0], [int(width), 0], [int(width), int(height)],[0, int(height)]])
    matrix = cv2.getPerspectiveTransform(src, dst)

    out = cv2.warpPerspective(imagen3, matrix, (int(width), int(height)))

    cv2.imwrite('perspectiva.png', out)


img = cv2.imread('casa.png')
orden = cv2.imread('orden.png')
sub_img = img.copy()
cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_circle)
perspectiva()
img_perspectiva = cv2.imread('perspectiva.png')
img_pers_copia = img_perspectiva
img = sub_img.copy()
cv2.imshow('img pers', img_perspectiva)
print("Presionar R para reiniciar las mediciones")
while 1:
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xFF

    if k == ord('m'):
        medicion = not medicion
    elif k == 27:
        break
    elif k == ord('q'):
        break
    elif k == ord('h'):
        perspectiva()
        img_perspectiva = cv2.imread('perspectiva.png')
        img_pers_copia = img_perspectiva
        img = sub_img.copy()
        cv2.imshow('imagen patron', img_perspectiva)
    elif k == ord('r'):
        img = sub_img.copy()
        contador = 0

cv2.destroyAllWindows( )
