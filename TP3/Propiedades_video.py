#!/usr/bin/env python
#-*-coding:utf-8-*-


import sys
import cv2
if(len(sys.argv) > 1):
	filename = sys.argv[1]
else:
	print('Escribir el nombre del archivo en el primer argumento')
	sys.exit(0)

cap = cv2.VideoCapture(filename)

fourcc = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')

framesize = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))


out = cv2.VideoWriter('output.avi', fourcc, 20.0, framesize, 0)

delay = int(cap.get(cv2.CAP_PROP_FPS))



while(cap.isOpened()):
	ret, frame = cap.read()
	if ret is True:
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		out.write(gray)
		cv2.imshow('Imagen Gris', gray)		
		if (cv2.waitKey(delay) & 0xFF) == ord('q'):			
			break
	else:
		break


cap.release()
out.release()
cv2.destroyAllWindows()
